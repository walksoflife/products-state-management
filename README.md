# Fetching products, use bloc/cubit to manage state

## How to run this project

Clone this repository using following command.

```
git@gitlab.com:walksoflife/products-state-management.git
```

Or

```
git clone https://gitlab.com/walksoflife/products-state-management.git
```

Then navigate to the project and start the application using following command.

```
flutter run
```

Runs the app in the connected device or emulator.
