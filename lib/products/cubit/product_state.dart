import 'package:equatable/equatable.dart';
import 'package:products/model/product_model.dart';

abstract class ProductState extends Equatable {}

// init state, don't have data
class InitialState extends ProductState {
  @override
  List<Object> get props => [];
}

// loading state, while loading data
class LoadingState extends ProductState {
  @override
  List<Object> get props => [];
}

// loaded state, return data
class LoadedState extends ProductState {
  final List<ProductModel> products;

  LoadedState(this.products);

  @override
  List<Object> get props => [products];
}

// error state, return error occurs during loading data
class ErrorState extends ProductState {
  @override
  List<Object> get props => [];
}
