import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:products/products/cubit/product_state.dart';
import 'package:products/repo/products_repo.dart';

class ProductCubit extends Cubit<ProductState> {
  // define instance of ProductRepository that provide method to fetch data
  final ProductRepository repository;

  ProductCubit({required this.repository}) : super(InitialState()) {
    getProducts();
  }

  void getProducts() async {
    try {
      // loading
      emit(LoadingState());

      // fetching
      final products = await repository.getAllProducts();

      // loaded
      emit(LoadedState(products));
    } catch (e) {
    
      // throw error
      emit(ErrorState());
    }
  }
}
