import 'package:flutter/material.dart';
import 'package:products/model/product_model.dart';
import 'package:products/constants.dart';

class ListProducts extends StatelessWidget {
  // define variable to store list products
  final List<ProductModel> listProducts;

  const ListProducts({super.key, required this.listProducts});

  /*
    Widget render list products.
    - Each product is a column includes : id, sku, name, desc 
  */
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: listProducts.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('id: ${listProducts[index].id.toString()}'),
              Text('sku: ${listProducts[index].sku.toString()}'),
              Text('name: ${listProducts[index].name.toString()}'),
              Text(
                  'desc: ${listProducts[index].desc.replaceAll(RegExp(htmlRegex), '')}')
            ],
          ),
        );
      },
    );
  }
}
