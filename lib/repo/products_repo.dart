// fetch all categories
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:products/constants.dart';
import 'package:products/model/product_model.dart';

class ProductRepository {
  Future<List<ProductModel>> getAllProducts() async {
    // productsUrl got from file constants.dart
    const url = productsUrl;

    // send req to url
    final response = await http.get(Uri.parse(url));

    // success => handle data
    if (response.statusCode == 200) {
      // convert json to object
      final jsonData = jsonDecode(response.body);

      // get list products
      final results = jsonData['products'] as List;

      // map list to convert each json object to object of ProductModel
      final data = results
          .map((p) => ProductModel(
              id: p['id'],
              sku: p['sku'],
              name: p['name'],
              thc: p['thc'],
              desc: p['desc']))
          .toList();

      return data;
    } else {
      // throw error
      throw Exception('Failed to load products');
    }
  }
}
