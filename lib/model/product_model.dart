class ProductModel {
  final int id;
  final String sku;
  final String name;
  final String thc;
  final String desc;
  // final String? cat;
  // final String? tags;
  // final String? weight;
  // final int? qTy;
  // final String? img;
  // final String? price;
  // final String? specialPrices;

  const ProductModel({
    required this.id,
    required this.sku,
    required this.name,
    required this.thc,
    required this.desc,
    // this.cat,
    // this.tags,
    // this.weight,
    // this.qTy,
    // this.img,
    // this.price,
    // this.specialPrices
  });
}
