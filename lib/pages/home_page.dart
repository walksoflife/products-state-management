import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:products/products/cubit/product_cubit.dart';
import 'package:products/products/cubit/product_state.dart';
import 'package:products/products/views/list_products.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('fetch products with cubit'),
      ),
      body: BlocBuilder<ProductCubit, ProductState>(
        builder: (context, state) {
          // check if state is LoadingState => loading progress
          if (state is LoadingState) {
            return const Center(child: CircularProgressIndicator());

          // check if state is ErrorState => text error
          } else if (state is ErrorState) {
            return const Text('Error occurred while loading the product list');

          // check if state is LoadedState => get data from state
          } else if (state is LoadedState) {
            final listProducts = state.products;

            // return layout display data
            return ListProducts(listProducts: listProducts);
          } else {
            return Container();
          }
        },
      ),
    );
  }
}
